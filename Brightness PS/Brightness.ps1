﻿$monitor = Get-WmiObject -ns root/wmi -class wmiMonitorBrightNessMethods

For ($i=0; $i -le 100; $i=$i+5) {
    $monitor.WmiSetBrightness(0,$i)    
}

For ($i=100; $i -gt 0; $i=$i-5) {
    $monitor.WmiSetBrightness(0,$i)    
}
