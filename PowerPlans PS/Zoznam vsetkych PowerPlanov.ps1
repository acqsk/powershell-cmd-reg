﻿# List of all PowerPlans
Get-WmiObject -Class Win32_PowerPlan -Namespace "root\cimv2\power" |
Select-Object @{Name = "IsActive"; Expression = {$_.IsActive}}, @{Name = "PowerPlan"; Expression = {$_.ElementName}}, @{Name = "InstanceID"; Expression = {$_.InstanceID}}

