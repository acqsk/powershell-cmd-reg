﻿:: EXPORT POWER PLANS...

SET batpath=%~dp0

mkdir "%batpath%\Exported"

:: Balanced
powercfg -export "%batpath%\Exported\Balanced.pow" 381b4222-f694-41f0-9685-ff5bb260df2e

:: High Performance
powercfg -export "%batpath%\Exported\High Performance.pow" 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

:: Power Saver
powercfg -export "%batpath%\Exported\Power Saver.pow" a1841308-3541-4fab-bc81-f71556f20b4a

:: IMPORT
:: powercfg -import "(Full Path of .pow file)"
:: Example: powercfg -import "%UserProfile%\Desktop\Custom Plan.pow"
